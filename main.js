/**
 * Created by Diana on 13.06.2016.
 */
/**(function ($) { var model = [{  fullName: "John",  description: "1, a road, a town, a county, a postcode",  fullDate: "1234567890",  deleteMe: function () { viewModel.info.remove(this); }
}], viewModel = { info: ko.observableArray(model)};

    ko.applyBindings(viewModel);

})(jQuery);

var Model = function () {
    this.name = ko.observable('');
    this.lastname = ko.observable('');
    this.description = ko.observable('');
    this.dateOfStart = ko.observable('');
    this.dateOfEnd = ko.observable('');
    this.fullName = ko.computed(function(){
      return this.name()+" "+this.lastname();

    },this)
    this.fullDate = ko.computed(function(){

        return this.dateOfStart()+" "+this.dateOfEnd();
    },this)
    this.notEmpty = ko.computed(function(){
        return !!this.name() || this.lastname() || this.description() || this.dateOfStart() || this.dateOfEnd
    },this);
    this.reset = function(){
        this.name (null);
        this.lastname (null);
        this.description (null);
        this.dateOfStart(null);
        this.dateOfEnd(null);
    };
    this.editName = function(){
        var input = document.getElementsByTagName('input')[0];
        input.focus()
        };
    this.editLastname = function(){
        var input = document.getElementsByTagName('input')[1];
        input.focus()
    };
    this.editDescription = function(){
        var input = document.getElementsByTagName('input')[2];
        input.focus()
    };
    this.editdateOfStart = function(){
        var input = document.getElementsByTagName('input')[3];
        input.focus()
    };
    this.editdateOfEnd = function(){
        var input = document.getElementsByTagName('input')[4];
        input.focus()
    };


}**/

 (function ($) { var model = [{  name: "John",  lastName: "1, a road, a town, a county, a postcode",  description: "1234567890",  dateOfStart: "www.ruseller.com", dateOfEnd: "/img/john.jpg",  deleteMe: function () { viewModel.people.remove(this); }
    }], viewModel = { people: ko.observableArray(model),displayButton: ko.observable(true),
     displayForm: ko.observable(false),
     showForm: function () {
         viewModel.displayForm(true).displayButton(false);
     },
     hideForm: function () {
         viewModel.displayForm(false).displayButton(true);
     },
     addPerson: function () {
         viewModel.displayForm(false).
             displayButton(true).
             people.push({
                 name: $("#name").val(),
                 lastName: $("#lastName").val(),
                 description: $("#description").val(),
                 dateOfStart: $("#dateOfStart").val(),
                 dateOfEnd: $("#dateOfEnd").val(),
                 deleteMe: function () { viewModel.people.remove(this); }
             });
     }
 };

    ko.applyBindings(viewModel);

})(jQuery);

